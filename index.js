/**
 * Created by dzambuto on 14/09/15.
 */
var debug = require('debug')('micro:mailer');
var Queue = require('bull');
var config = require('config');

var default_port = 6379;
var default_host = '127.0.0.1';
var instance;

if (config.has('main.redis')) {
  var redis = config.get('main.redis');
  default_port = redis.port;
  default_host = redis.host;
}

function Mailer (options) {
  options = options || {};

  this.prefix = options.prefix || 'mailer:';
  this.name = options.name || 'main';

  this.queue = Queue(
    this.prefix + this.name,
    options.port || default_port,
    options.host || default_host
  );
}

Mailer.prototype.send = function (mail, fn) {
  var queue = this.queue;

  if (mail.email) {
    debug('create job to send email to %s with payload %s', mail.email, JSON.stringify(mail));
    queue
      .add(mail)
      .then(function () {
        debug('job created');
        return fn();
      })
      .catch(fn);
  }
};

var mailer = module.exports = exports = new Mailer;
